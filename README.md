# fimber_catcher_magic_app

A basic setup of build variants, fimber logging using flutter log view and 
catcher app exception reporting.

See templates for the build variant part as build vaqriants are gitignored except for  ci server to 
prevent api keys from being stored in git.

# Resources

[Fimber](https://github.com/magillus/flutter-fimber)

[catcher](https://github.com/jhomlala/catcher)

[Error reporting in flutter](https://blog.bam.tech/developer-news/error-reporting-in-flutter)


# Credits

The small catcher addition about exception halding app wise, see

[Error reporting in flutter](https://blog.bam.tech/developer-news/error-reporting-in-flutter)

My stuff, Fredirck Grott

# License

BSD clause 2 copyright 2019 Fredirck Grott